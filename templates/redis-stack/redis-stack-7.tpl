<@requirement.NODE 'redis' 'redis-${namespace}' />

<@requirement.PARAM name='PUBLISHED_PORT' type='port' required='false' description='Specify postgres external port (for example 6379)' />
<@requirement.PARAM name='PUBLISHED_PORT_TYPE' values='global,local' value='global' type='select' depends='PUBLISHED_PORT' />

<@requirement.PARAM name='REDIS_ARGS' value='--appendonly yes --aof-use-rdb-preamble yes --appendfsync always --protected-mode no --maxmemory 1gb' />
<@requirement.PARAM name='REDISEARCH_ARGS' required='false' />
<@requirement.PARAM name='REDISJSON_ARGS' required='false' />

<@img.TASK 'redis-${namespace}' 'imagenarium/redis-stack-server:7.4.0-v1'>
  <@img.NODE_REF 'redis' />

  <@img.PORT PARAMS.PUBLISHED_PORT '6379' PARAMS.PUBLISHED_PORT_TYPE />

  <@img.VOLUME '/data' />

  <@img.BIND '/sys/kernel/mm/transparent_hugepage' '/thp' />

  <@img.ULIMIT 'nofile=65536:65536' />
  <@img.ULIMIT 'nproc=4096:4096' />
  <@img.ULIMIT 'memlock=-1:-1' />

  <@img.SYSCTL 'net.core.somaxconn' '4096' />
  <@img.CUSTOM '--privileged=true' />

  <@img.ENV 'REDIS_ARGS' PARAMS.REDIS_ARGS />
  <@img.ENV 'REDISEARCH_ARGS' PARAMS.REDISEARCH_ARGS />
  <@img.ENV 'REDISJSON_ARGS' PARAMS.REDISJSON_ARGS />

  <@img.CHECK_PORT '6379' />
</@img.TASK>
